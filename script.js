console.log("JavaScriptが読み込まれました!");

let vhToPixel = function(value){
  return $(window).height() / 100 * value; //指定された数にブラウザの表示領域の高さ/100をかける
}

// let touchY;
//
// $('body').on('touchstart', function(e){
//   touchY = e.pageY;
// });
//
$('body').on('touchmove', function(e){
  if( $(window).scrollTop() < 0 )                  　　　　　　　　　　　　　e.preventDefault();
  if( $(window).scrollTop() + $(window).height() > $('body').height()　) e.preventDefault();
});

$('body').on('touchend', function(){
  if( $(window).scrollTop() < 0 )                  　　　　　　　　　　　　　$(window).scrollTop(0);
  if( $(window).scrollTop() + $(window).height() > $('body').height()　) $(window).scrollTop( $('body').height() );
});

let parallaxRender = function(){

  let scroll = $(this).scrollTop();
  let parallax = 0.5; //大きい方が遠い、　スクロールが遅くなる
  $('p').text(scroll);
  $('#foundation').css('background-position', '50% '+scroll*parallax+'px');

  let scrollSpeed = 0.3; //パーセントで指定（25% = 0.25) 大きい方が遅い、遠い
  {
    for(let loop = 0; loop < 20; loop += 2){

      // console.log('#イベント発火') //デバッグ用;

      let element = $('.element').eq(loop); //1個目の要素を使う
      let elementHeight = element.css('height'); //要素の高さを入れる
      let elementTop = element.css('top'); //要素のcssのtopを入れる

      if(elementHeight.match(/vh/)){ //Heightの文字列にvhが含まれたいたら（単位がvhだったら）
        elementHeight = vhToPixel( Number( elementHeight.replace(/vh/, '') ) ); //Heightの文字列からvhを消して、数値に変換して、vhからpxに変換
      }
      if(elementHeight.match(/px/)){ //ちがう、そうじゃない。って感じだったら
        elementHeight = Number( elementHeight.replace(/px/, '') ); //そのまま数値に変換
      }
      if(elementTop.match(/vh/)){ //Topの文字列にvhが含まれたいたら（単位がvhだったら）
        elementTop = vhToPixel( Number( elementTop.replace(/vh/, '') ) ); //Topの文字列からvhを消して、数値に変換して、vhからpxに変換
      }
      if(elementTop.match(/px/)){ //ちがう、そうじゃない。って感じだったら
        elementTop = Number( elementTop.replace(/px/, '') ); //そのまま数値に変換
      }

      let elementCenter = ( element.offset().top - elementTop ) + elementHeight / 2; //要素の左上の絶対Y座標からtopの値を引いて出した、もともとの座標に、要素の高さの半分を足して、中心点の絶対Y座標をだす
      let screenCenter = $(window).scrollTop() + $(window).height() / 2; //上と同じことして表示領域の中心点のYだす
      let difference = screenCenter - elementCenter; //差をだす。　要素の中心点が、表示領域の中心点より下（Y座標が高い）だったら値が＋になる

      //要素のTopをいじってパララックスぽっくする
      // if(scrollSpeed >= 1) scrollSpeed -= 1;
      // if(scrollSpeed < 1 ) scrollSpeed = 1 / scrollSpeed;
      element.css('top', String(difference * scrollSpeed) + 'px');

      //デバッグ用
      // console.log('elementHeight: '+String(elementHeight));
      // console.log('elementCenter: '+String(elementCenter));
      // console.log('screenCenter: '+String(screenCenter));
      // console.log(String(loop)+': difference: '+String(difference));
      // console.log('scrollSpeed:' +String(scrollSpeed));

    }
  }

}

if (window.ontouchstart !== null) {
    $('body').niceScroll();
}

parallaxRender();
$(window).on('scroll', parallaxRender);


/*iOS10のSafariで複数指で拡大できてしまうのを防ぐ*/
/*この文がプログラムの最後にくるようにする*/

$("#foundation").on("touchend", function(event){
    event.preventDefault();
});
